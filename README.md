// Purpose
For self enrichment on more hands on practice with Spring, Angular, and othe
Types of Databases.

Technologies

///// Back End
- Java

Framework
- Spring
- Spring Boot
- Spring Data

Dependency Injection
- Maven


///// Front End
- TypeScript
- Angular7

///// DevOps
- AWS
- EC2
- Jenkins
- S3
- Docker
- Git

///// RDBMS
- MongoDB (NoSQL)

Unit Testing
- JUnit (BackEnd)
- Selenium (Front End)
